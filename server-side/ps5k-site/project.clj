(defproject ps5k-site "0.1.0-SNAPSHOT"
  :description "Website for the Stetson Fall 2017 Software Development II class project, Planet Simulator 5K."
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.6.0"]
                 [ring/ring-defaults "0.2.1"]
                 [proto-repl "0.3.1"]
                 [hiccup "1.0.5"]
		 [korma "0.4.3"]
		 [mysql/mysql-connector-java "5.1.36"]
		 [propertea "1.2.3"]
		 [org.clojure/data.json "0.2.6"]]
  :plugins [[lein-ring "0.12.1"]]
  :ring {:handler ps5k-site.handler/app}
  :main ps5k_db.core
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
