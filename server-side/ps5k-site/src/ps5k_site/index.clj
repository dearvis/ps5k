(ns ps5k-site.index
  (require [hiccup.core :refer [html]])
  (require [hiccup.page :refer [include-css]])
  (require [hiccup.page :refer [include-js]])
  (require [hiccup.element :refer [link-to]])
  (require [hiccup.form :refer [form-to]])
  (require [hiccup.element :refer [image]])
  (require [hiccup.util :refer [to-uri]]))

(defn set-utf-8
  []
  [:meta {:charset "UTF-8"}])

(defn top-nav
  []
  [:div.topnav
   (for [[href item] [["/" "Home"]
                      ["/planets" "Planets!!"]
                      ["/" "About"]]]
      (link-to href item))])

(defn index
  []
  (html
   [:head
    (include-css "style.css")
    (set-utf-8)
    [:title "Home | Planet Simulator 5K"]
    (top-nav)]
   [:body
    [:div#header [:h1 "Planet Simulator 5K!"]]
    [:ul
     (for [item ["lions" "tigers" "bears"]]
       [:li item])]]))

(def search-options
  {:byplanet "By Planet"
   :byuser "By User"})

(defn searchtypebutton
  [dfault]
  [:button#searchtype.btn (dfault search-options)])

(defn planet-list
  []
  (html
   [:head
    (include-css "style.css")
    (set-utf-8)
    (top-nav)
    [:title "Planets!! | Planet Simulator 5K"]]
   [:body
    [:h1 "Planet Models"]
    [:p "These are planet models built
         by our users using the game!"]
    (searchtypebutton :byplanet)
    (form-to [:get "/planet"]
     [:input#planetsearchbar
      {:type "text" :name "planet" :placeholder "Search Planets:"}])
    (form-to [:get "/user"]
      [:input#usersearchbar
       {:type "text" :name "user" :placeholder "Search Users:"}])]))

(defn search-planets
  [planet]
  (html
   [:head
    (include-css "style.css")
    (set-utf-8)
    (top-nav)
    [:title (str planet " | Planet Simulator 5K")]]
   [:body
    [:div.center
     [:img.planetimage {:src (to-uri "http://www.pngall.com/wp-content/uploads/2016/06/Earth-PNG-Pic.png"),
                        :alt "Planet!"}]
     [:div.paddeddown 
      [:h1 planet]
      [:p "Created by user: Hobo1"]]]]))

(defn search-users
  [user]
  (html
   [:head
    (include-css "style.css")
    (set-utf-8)
    (top-nav)
    [:title (str user " | Planet Simulator 5K")]]
   [:body
    [:h1 user]
    [:div.center
     [:p "Number of planets: 1"]
     [:p "Planet(s):"]]]))
