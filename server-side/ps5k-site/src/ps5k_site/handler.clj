(ns ps5k-site.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [hiccup.core :refer [html]])
  (:require [ps5k-site.index :as index]))

(defroutes app-routes
  (route/resources "/")
  (GET "/" [] (index/index))
  (GET "/planets" [] (index/planet-list))
  (GET "/planet" [planet] (index/search-planets planet))
  (GET "/user" [user] (index/search-users user))
  ;; Ask eckroth about this (GET "/planets/?" [] (index/planet-list))
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))
