(ns ps5k_db.core
  (:use [korma.db :only [defdb mysql]])
  (:use [korma.core])
  (:use propertea.core)
  (:require [clojure.data.json :as json]))
  
(declare users planets)

(defentity users
  (has-many planets))

(defentity planets
  (belongs-to users))

(defn add-user
  [username email password]
  (try
   (insert users (values [{:username username :email email :password password}]))
   (catch com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException _
     (println "Failed to insert user. Username or E-mail already in use."))))


(defn delete-user
  [username password]
  (delete users (where {:username username :password password})))

(defn get-user
  [username]
  (select users (where {:username username})))


(defn add-planet
  [planetname userid planetinfo planetmodel]
  (try
    (insert planets (values [{:planetname planetname :userid userid :planetinfo planetinfo :planetmodel planetmodel}]))
    (catch com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException __
      (println "Failed to insert planet. Name already in use."))))


(defn delete-planet
  [planetname userid]
  (delete planets (where {:planetname planetname :userid userid})))

(defn get-planet
  [planetname]
  (select planets (where {:planetname planetname})))


(defn setup-db
  [dbname user password]
  (defdb maindb (mysql {:db dbname :user user :password password})))
 
(defn db-init
  []
  (println "Setting up db...")
  (let [props (read-properties "././resources/private/ps5kdb.properties")]
    (setup-db (:dbname props) (:user props) (:password props))))



