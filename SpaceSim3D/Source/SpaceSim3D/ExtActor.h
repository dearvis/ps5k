// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "csci321-ps5k/PhysicsEqs/Equations.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExtActor.generated.h"

UCLASS()
class SPACESIM3D_API AExtActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExtActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	float testFloatOV;

	
	
};
