// Fill out your copyright notice in the Description page of Project Settings.

#include "ExtActor.h"


// Sets default values
AExtActor::AExtActor()
{
	Equations eq = new Equations(100.0);	
	testFloat = eq.getOrbitalVelocity();
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AExtActor::BeginPlay()
{
	
	Super::BeginPlay();
	
}

// Called every frame
void AExtActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

