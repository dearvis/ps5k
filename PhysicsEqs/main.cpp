#include <iostream>
#include "equations.h"
using namespace std;

int main()
{
    Equations a(100.0); 
    cout << "Orbital Radius = " << a.getOrbitalRadius() << endl;
    cout << "Period Orbit = " << a.getPeriodOrbit() << endl;
    cout << "Orbital Velocity = " << a.getOrbitalVelocity() << endl;
    cout << "Gravitational Acceleration = " << a.getGravitationalAcceleration() << endl;

    return 0;
}
