#include "equations.h"

#include <map>
#include <iostream>
#include <cassert>
#include <cmath>
using namespace std;

Equations::Equations(float sunMass) {
  this -> sunMass = sunMass;
  gravitationalConstant = 6.67408 * pow(10, -2);
  pi = M_PI;
  boltzmannConstant = 1.38064852 * pow(10, -17);
}

float Equations::getOrbitalVelocity() {
    return sqrt((gravitationalConstant * sunMass) / getOrbitalRadius());
}

float Equations::getGravitationalAcceleration() {
    return (gravitationalConstant * sunMass) / pow(getOrbitalRadius(), 2);
}

float Equations::getPeriodOrbit() {
    return 2 * pi * getOrbitalRadius() * sqrt(getOrbitalRadius() / gravitationalConstant * sunMass);
}

// user can set sunMass
float Equations::getOrbitalRadius() {
    return pow(((getPeriodOrbit() * sqrt(gravitationalConstant * sunMass)) / 2 * pi), (2/3));
}

// hashmap for albedo & emissivity here:

float Equations::getAlbedo(string key) {

    map<string, int> m;
    m["snow old"] = 0.4;
    m["snow fresh"] = 0.85;
    m["ice"] = 0.36;
    m["dry sand"] = 0.4;
    m["wet sand"] = 0.24;
    m["water"] = 0.06;
    m["forest"] = 0.1;
    m["savanna"] = 0.17;
    m["meadows"] = 0.14;
    m["crops"] = 0.2;
    m["dry soil"] = 0.28;
    m["desert"] = 0.275;
    m["stratus"] = 0.4;
    m["cumulus stratus"] = 0.7;
    m["altostratus cirrus"] = 0.5;    
  
    // check if key is present
    if (m.find(key) != m.end()) {
        cout << "map contains key!\n";
        
        // retrieve
        cout << m[key] << '\n';
               map<string, int>::iterator i = m.find(key);
                 assert(i != m.end());
               cout << "Key: " << i->first << " Value: " << i->second << '\n';
    }

    return m[key];
}

// research how planet forms
// select list of what users can pick from


float Equations::getHeatOfBlackbody(float radiatingArea, float time, float temp) {
    return getEmissivity() * boltmannConstant * getRadiatingArea() * time * temp;
}

float Equations::getSurfaceTemp(float areaAbsorbs, float areaRadiated, float luminosity, float albedo, float distance) {
    return pow(((1/2) * ((luminosity * (1 - albedo)) / 4 * pi * boltzmannConstant * getEmissivity * distance * distance)), (1/4));   
}

float Equations::getRadiatingArea(float planetRadius) {
    return 4*pi*pow(planetRadius,2);
}

Equations::~Equations() {
    delete sunMass;
    delete gravitationalConstant;
    delete pi;
}
