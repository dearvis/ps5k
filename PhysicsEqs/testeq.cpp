#include <iostream>
#include <cmath>
#include "equations.h"
#include "gtest/gtest.h"
using namespace std;

TEST(equations, formulas)
{
    Equations eq(100.00);
    ASSERT_EQ(eq.getOrbitalVelocity(), sqrt((eq.gravitationalConstant * 100.00) / eq.getOrbitalRadius()));
    ASSERT_EQ(eq.getGravitationalAcceleration(), (eq.gravitationalConstant * 100.00) / pow(eq.getOrbitalRadius(), 2));
    ASSERT_EQ(eq.getPeriodOrbit(), 2 * eq.pi * eq.getOrbitalRadius() * sqrt(eq.getOrbitalRadius() / eq.gravitationalConstant * 100.00));
    ASSERT_EQ(eq.getOrbitalRadius(), pow(((eq.getPeriodOrbit() * sqrt(eq.gravitationalConstant * 100.00)) / 2 * eq.pi), (2/3)));

}
