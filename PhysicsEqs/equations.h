#ifndef EQUATIONS_H
#define EQUATIONS_H

#include <iostream>
using namespace std;

class Equations {
  
  public:
    Equations(float bodyMass);
    
    float getOrbitalVelocity();
    float getGravitationalAcceleration();
    float getPeriodOrbit();
    float getOrbitalRadius();
    float getRadiatingArea();
    
    float pi;
    float gravitationalConstant;
    
    ~Equations();
     
  private:
    float bodyMass;
};

#endif
